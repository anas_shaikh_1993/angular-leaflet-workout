import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletDrawModule } from '@asymmetrik/ngx-leaflet-draw';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";


import { AppComponent } from './app.component';
import { ZoneDrawLayoutComponent } from './zone-draw-layout/zone-draw-layout.component';

@NgModule({
  declarations: [
    AppComponent,
    ZoneDrawLayoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    LeafletModule,
    LeafletDrawModule ,
    GooglePlaceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
