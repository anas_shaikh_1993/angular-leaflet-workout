import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZoneDrawLayoutComponent } from './zone-draw-layout.component';

describe('ZoneDrawLayoutComponent', () => {
  let component: ZoneDrawLayoutComponent;
  let fixture: ComponentFixture<ZoneDrawLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZoneDrawLayoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZoneDrawLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
