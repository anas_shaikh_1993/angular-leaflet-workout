import { Component, OnInit,ViewChild ,AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { DrawEvents, featureGroup, FeatureGroup, icon, latLng, tileLayer,marker,polygon,LatLngExpression } from 'leaflet';
import { GooglePlaceDirective } from "ngx-google-places-autocomplete";
import { Address as googleAddressObj } from "ngx-google-places-autocomplete/objects/address";
import { nanoid } from 'nanoid'


//do not remove this snippet. Important to remove clear all button from the ui layout
L.EditToolbar.Delete.include({
  removeAllLayers: false
});

//Dummy jsonData acting as a proxy data from server for now
const serverResponse = [
  {
      'id':12,
      'lookupId':'dpfertIN6UqhijxmKH0mq',
      'session_id_of_shape': 179,
      'created_by_id':3,
      'hotspot_description': "gagag",
      'hotspot_name': "faf",
      'geoJsonString': '{"type":"Feature","properties":{},"geometry":{"type":"Polygon","coordinates":[[[72.818091,18.969352],[72.818091,18.96989],[72.819099,18.96989],[72.819099,18.969352],[72.818091,18.969352]]]}}'
  }
];


//Important Note . Do not delete
// https://stackoverflow.com/questions/24018630/how-to-save-a-completed-polygon-points-leaflet-draw-to-mysql-table
/*
map.on('draw:created', function (e) {
  var type = e.layerType;
  var layer = e.layer;

  var shape = layer.toGeoJSON()
  var shape_for_db = JSON.stringify(shape);
});

// restore
L.geoJSON(JSON.parse(shape_for_db)).addTo(mymap);
*/

interface Zones{
  type:string;
  radius?:number;
  latlngs:number[];
}

const CIRCLE_TYPE = "circle"
const POLYGON_TYPE = "polygon"

@Component({
  selector: 'zone-draw-layout',
  templateUrl: './zone-draw-layout.component.html',
  styleUrls: ['./zone-draw-layout.component.css']
})
export class ZoneDrawLayoutComponent implements OnInit ,AfterViewInit  {
  @ViewChild("placesRef") placesRef : GooglePlaceDirective;
  map: L.Map;
  
  //form fields
  hotspot_name:string = "";
  hotspot_description:string = "";
  created_by_id:number = 0;
  disabledMap:boolean = true;
  showSaveButton:boolean = false
  constructor() { }

  shapeToInsert = {}

  //keep  it object . traverse it using object.keys. follow the principle of normalization and fast data lookup 
  ZonesEntitiesLookUpTable = {};
  //keep it object. Mapper of sessionId with lookupId
  sessionIdToLookerIdMapper = {}

  arrShapes = [];

  private latlngs:LatLngExpression[] = [[37, -109.05],[41, -109.03],[41, -102.05],[37, -102.04]];



  summit = marker([ 46.8523, -121.7603 ], {
    icon: icon({
      iconSize: [ 25, 41 ],
      iconAnchor: [ 13, 41 ],
      iconUrl: 'leaflet/marker-icon.png',
      shadowUrl: 'leaflet/marker-shadow.png'
    })
  }).bindPopup('Hi Welcome to Tutorialspoint').openPopup();

  polygon = polygon(this.latlngs, {color: 'red'});

  //initially just show a map . create a variable to toogle
  justShowMap:boolean = true;

  ngOnInit(): void {
    // console.log("on view init")
  }

  ngAfterViewInit():void {
      console.log("after view init")
      serverResponse.forEach(item => {
        // console.log(item);
        const  myjsonString:string = item['geoJsonString'];
        let geolayer = L.geoJSON(JSON.parse(myjsonString))
        geolayer.getLayers().forEach(ele => {
          if(ele instanceof L.Polygon)
          {

             let newLayer:L.Polygon = ele;
            //important piece of hack
            // do not consider this id as permanent . its only available for a session
            const session_id_of_shape = (L.Util.stamp(newLayer));

             newLayer.bindPopup(item['hotspot_name']).bindTooltip((item['hotspot_name']));
             this.drawnItems.addLayer(newLayer)
              //important piece of hack .
              item['session_id_of_shape'] = session_id_of_shape;
              this.ZonesEntitiesLookUpTable[item['lookupId']] = item;
              this.sessionIdToLookerIdMapper[session_id_of_shape] = item['lookupId']
          }
          else if(ele instanceof L.Circle)
          {

             let newLayer:L.Circle = ele;
            //important piece of hack
            // do not consider this id as permanent . its only available for a session
            const session_id_of_shape = (L.Util.stamp(newLayer));

             newLayer.bindPopup(item['hotspot_name']).bindTooltip((item['hotspot_name']));
             this.drawnItems.addLayer(newLayer)
              //important piece of hack .
              item['session_id_of_shape'] = session_id_of_shape;
              this.ZonesEntitiesLookUpTable[item['lookupId']] = item;
              this.sessionIdToLookerIdMapper[session_id_of_shape] = item['lookupId']
          }
        })
        
     
        console.log(this.ZonesEntitiesLookUpTable)

        // //important piece of hack
        // // do not consider this id as permanent . its only available for a session
        // const session_id_of_shape = (L.Util.stamp(layer));


        // // layer.bindPopup(item['hotspot_name']).bindTooltip((item['hotspot_name']));
        // this.drawnItems.addLayer(layer)
        // //important piece of hack .
        // item['session_id_of_shape'] = session_id_of_shape;
        // this.ZonesEntitiesLookUpTable[item['lookupId']] = item;
      })
  }

  drawnItems: FeatureGroup = featureGroup();
  
	options = {
		layers: [
			tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'Open Street Map' })
		],
		zoom: 5,
		center: latLng({ lat: 46.879966, lng: -121.726909 })
  };

  

  
  onMapReady(map:L.Map) {
    // Do stuff with map
    this.map = map;

    // this.map.dragging.disable();
    // this.map.touchZoom.disable();
    // this.map.doubleClickZoom.disable();
    // this.map.scrollWheelZoom.disable();
    // this.map.boxZoom.disable();
    // this.map.keyboard.disable();
    // if (this.map.tap) this.map.tap.disable();
    // document.getElementById('map').style.cursor='default';
    // document.querySelectorAll<HTMLElement>(".leaflet-draw-toolbar a").forEach(function(elem) { 
    //   elem.style.backgroundImage = "linear-gradient(transparent,transparent),url('/path/to/greyed-out/spritesheet.svg')" 
    //   elem.style.pointerEvents = "none";
    // })
    
    
  }

	drawOptions = {
		position: 'topright',
		draw: {
      marker: false,
      polyline: false,
      circlemarker:false
		},
		edit: {
      featureGroup: this.drawnItems,
      // remove: true,
      // removeAllLayers:false
    },
    
  };

  
  
  

	drawLocal: any = {
		draw: {
     	toolbar: {
				buttons: {
          polygon: 'create zone using polygon!',
          circle: 'create zone using circle',
          rectangle:'create zone using rectange'
				}
			}
    },
    edit:{
      toolbar:{
        actions: {
          save: {
            title: 'Save changes.',
            text: 'Save'
          },
          cancel: {
            title: 'Cancel editing, discards all changes.',
            text: 'Cancel'
          },
          
        },
      }
    }
  };
  

  
  

	public onDrawCreated(e: any) {
    let objToInsert = {};
    
		// tslint:disable-next-line:no-console
    // console.log('Draw Created Event!');
    
    

    const layer = (e as DrawEvents.Created).layer;
    // do not consider this id as permanent . its only available for a session
    const session_id_of_shape = (L.Util.stamp(layer));
    //create a randomId which will be a original  lookupId for the statemanagement
    const lookupId = nanoid();
    objToInsert['session_id_of_shape'] = session_id_of_shape;
    objToInsert['lookupId'] = lookupId;
    objToInsert['hotspot_name'] = this.hotspot_name;
    objToInsert['hotspot_description'] = this.hotspot_description;
    objToInsert['created_by_id'] = this.created_by_id;
    
    // console.log(L.Util.stamp(layer))
    if (layer instanceof L.Circle)
    {
      let circle:L.Circle = layer;
      // console.log(circle.getRadius());
      // console.log(circle.getLatLng());
      objToInsert['geometry'] = {'type':CIRCLE_TYPE,'radius':circle.getRadius(),'latlng':circle.getLatLng()};
      objToInsert['geoJson'] = circle.toGeoJSON().geometry;
      objToInsert['geoJsonString'] = JSON.stringify(circle.toGeoJSON())
    }else if(layer instanceof L.Polygon)
    {
      let polygon:L.Polygon = layer;
      objToInsert['geometry'] = {'type':POLYGON_TYPE,'latlng':polygon.getLatLngs()};
      objToInsert['geoJson'] = polygon.toGeoJSON().geometry;
      objToInsert['geoJsonString'] = JSON.stringify(polygon.toGeoJSON())
      // console.log(polygon.toGeoJSON().geometry)
    }
    
    
    // console.log(layer['_leaflet_id'])
    //adding properties
    layer.bindPopup(this.hotspot_name).bindTooltip(this.hotspot_description);
    

    // console.log(layer)
    // var shape = layer.toGeoJSON()
    // console.log(shape);
    
    // var shape_for_db = JSON.stringify(shape);
    // this.arrShapes.push(shape_for_db);
    // console.log(shape)
    // console.log(shape_for_db)
    this.drawnItems.addLayer(layer);
    this.shapeToInsert = objToInsert;
	}

	public onDrawEdited(e: any) {
    // console.log((e as DrawEvents.Edited).type);
    // console.log((e as DrawEvents.Edited).target);
    // console.log((e as DrawEvents.Edited).sourceTarget);
    // console.log((e as DrawEvents.Edited).propagatedFrom);
    // console.log((e as DrawEvents.Edited).layers);
    const editedlayers = (e as DrawEvents.Edited).layers;
    editedlayers.eachLayer(layer => {
      const session_id =  L.Util.stamp(layer)
      // console.log("id:::" + L.Util.stamp(layer))
      if (layer instanceof L.Circle)
      {
        let circle:L.Circle = layer;
        let entityObject = this.ZonesEntitiesLookUpTable[this.sessionIdToLookerIdMapper[session_id]]
        entityObject['geometry'] = {'type':CIRCLE_TYPE,'radius':circle.getRadius(),'latlng':circle.getLatLng()};
        entityObject['geoJson'] = circle.toGeoJSON().geometry;
        entityObject['geoJsonString'] = JSON.stringify(circle.toGeoJSON())
        this.ZonesEntitiesLookUpTable[this.sessionIdToLookerIdMapper[session_id]] = entityObject;
        // console.log(circle.getRadius());
        // console.log(circle.getLatLng());
      }else if(layer instanceof L.Polygon)
      {
        let polygon:L.Polygon = layer;
        // console.log(polygon.toGeoJSON().geometry)
        let entityObject = this.ZonesEntitiesLookUpTable[this.sessionIdToLookerIdMapper[session_id]]
        entityObject['geometry'] = {'type':POLYGON_TYPE,'latlng':polygon.getLatLngs()};
        entityObject['geoJson'] =  polygon.toGeoJSON().geometry;;
        entityObject['geoJsonString'] = JSON.stringify(polygon.toGeoJSON())
        this.ZonesEntitiesLookUpTable[this.sessionIdToLookerIdMapper[session_id]] = entityObject;
      }
      // var shape_for_db = JSON.stringify(shape);
    });

    // console.log("draw edited event callled");
    // console.log(e);
    // var editedlayers = e.layers;
    //     editedlayers.eachLayer(function(layer) { // Do something with the edited layer
    //       console.log(layer)
    //     });
		// tslint:disable-next-line:no-console
    // console.log('Draw Started Event!');
    
    console.log("-------Edit ZonesEntitiesLookUpTable ---------------");
    console.info(this.ZonesEntitiesLookUpTable)
    console.log("--------Edit ZonesEntitiesLookUpTable--------------");

    console.log("------- sessionIdToLookerIdMapper ---------------");
    console.info(this.sessionIdToLookerIdMapper);
    console.log("-------- sessionIdToLookerIdMapper --------------");
  }
  
  clickEvent(event){
    // console.log(this.arrShapes)
    this.drawnItems.addLayer(this.summit);
    this.drawnItems.addLayer(this.polygon);

  }

  changeView(event) {
    this.map.panTo(new L.LatLng(40.737, -73.923));
  }

  public handleAddressChange(address: googleAddressObj){
    console.log("-------------------------------")  
    console.log(address.geometry.location.lng());
    console.log(address.geometry.location.lat());
    console.log(address.geometry.location.toJSON());
    this.map.panTo(new L.LatLng(address.geometry.location.lat(), address.geometry.location.lng()));
    this.map.setZoom(20)
    console.log("-------------------------------")  
  }


  getListOfAllDrawnShapes(event){
    this.drawnItems.eachLayer(function (layer){
        console.log(layer);
    })
  }

  onDrawDeleted(e:any){
    console.log("---------------ZonesEntitiesLookUpTable -------------------")
    console.log(this.ZonesEntitiesLookUpTable)
    console.log("---------------ZonesEntitiesLookUpTable -------------------")
    console.log("---------------sessionLookUpMapper -------------------")
    console.log(this.sessionIdToLookerIdMapper)
    console.log("---------------sessionLookUpMapper -------------------")
    const deletedlayers = (e as DrawEvents.Deleted).layers;
    // console.log(deletedlayers)
    console.log(this.ZonesEntitiesLookUpTable);
    deletedlayers.eachLayer(layer => {
      console.log(layer)
      //get id of layer 
      const session_id_of_shape = (L.Util.stamp(layer));
      this.drawnItems.removeLayer(layer);
       
       delete this.ZonesEntitiesLookUpTable[this.sessionIdToLookerIdMapper[session_id_of_shape]]
       delete this.sessionIdToLookerIdMapper[session_id_of_shape]
    });

    console.log("---------------After ZonesEntitiesLookUpTable -------------------")
    console.log(this.ZonesEntitiesLookUpTable)
    console.log("---------------After ZonesEntitiesLookUpTable -------------------")
    console.log("---------------After sessionLookUpMapper -------------------")
    console.log(this.sessionIdToLookerIdMapper)
    console.log("---------------After sessionLookUpMapper -------------------")
  }

  onZoneDrawSubmit(e:any){
    console.log("this is submitted!!!")
    console.log(this.hotspot_name);
    console.log(this.hotspot_description);
    console.log(this.created_by_id);
    this.disabledMap = false;
    this.showSaveButton = true;

    // this.map.dragging.enable();
    // this.map.touchZoom.enable();
    // this.map.doubleClickZoom.enable();
    // this.map.scrollWheelZoom.enable();
    // this.map.boxZoom.enable();
    // this.map.keyboard.enable();
    // if (this.map.tap) this.map.tap.enable();
    this.justShowMap = false;

  }

  onZoneDrawSaveClickedEvent($event){
    console.log("onZoneDrawSaveClickedEvent")
    
    this.ZonesEntitiesLookUpTable[this.shapeToInsert['lookupId']] = this.shapeToInsert;
    this.sessionIdToLookerIdMapper[this.shapeToInsert['session_id_of_shape']] = this.shapeToInsert['lookupId']
    this.shapeToInsert = {};
    console.log(this.ZonesEntitiesLookUpTable)
  }


}
